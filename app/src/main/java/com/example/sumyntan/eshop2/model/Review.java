package com.example.sumyntan.eshop2.model;

/**
 * Created by sumyntan on 08/10/2016.
 */

public class Review {
    private int mRating;

    public Review(int rating) {
        mRating = rating;
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int rating) {
        mRating = rating;
    }
}

