package com.example.sumyntan.eshop2.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.model.Laptop;
import com.example.sumyntan.eshop2.model.Review;

/**
 * Created by sumyntan on 08/10/2016.
 */
import java.util.ArrayList;


public class LaptopFragment extends Fragment {
    private ListView mListView;
    private ArrayList<Laptop> mLaptopList;

    private String[] mLaptopNames = {
            "Acer",
            "HP",
            "ASUS",
            "Apple"
            };
    private String[] mColors = {
            "Silver",
            "Black",
            "White",
            "Red"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_laptop_listing, container, false);

        mListView = (ListView) rootView.findViewById(R.id.fragment_laptoplisting_listview);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLaptopList = new ArrayList<>();

        for (int i = 0; i < mLaptopNames.length; i++) {
            Laptop laptop = new Laptop(mLaptopNames[i], mColors[i]);
            laptop.setReviews(generateRandomReviewList());

            mLaptopList.add(laptop);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, mLaptopList);
        mListView.setAdapter(arrayAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Laptop selectedLaptop = mLaptopList.get(i);
                // Review for laptop
                // String snackbarMessage = "Average review of " + selectedLaptop.getName() + " is " + selectedLaptop.getAverageReview();
                String snackbarMessage = "Color Choose " + selectedLaptop.getColor() + "! Laptop Name " + selectedLaptop.getName();

                Snackbar.make(view, snackbarMessage, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Review> generateRandomReviewList() {
        ArrayList<Review> randomList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            randomList.add(new Review((int) (Math.random() * 5) + 1));
        }

        return randomList;
    }

}
