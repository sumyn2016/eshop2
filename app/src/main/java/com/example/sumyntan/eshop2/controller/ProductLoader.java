package com.example.sumyntan.eshop2.controller;

import android.content.Context;
import android.util.Log;

import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.model.Product;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by sumyntan on 08/10/2016.
 */

public class ProductLoader {
    private static final String TAG = "ProductLoader";

    public static List<Product> loadSamples(Context context) {
        ArrayList<Product> sampleProducts = new ArrayList<>();

        InputStream inputStream = context.getResources().openRawResource(R.raw.products);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(";");

                String dateString = tokens[0];
                String title = tokens[1];
                String body = tokens[2];

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar creationDate = Calendar.getInstance();
                creationDate.setTime(dateFormat.parse(dateString));

                Product journal = new Product(title, body, creationDate);
                sampleProducts.add(journal);
            }

        } catch (Exception exception) {

            Log.e(TAG, "Load exception: " + exception.toString());
        }

        return sampleProducts;
    }
}
