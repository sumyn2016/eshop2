package com.example.sumyntan.eshop2.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.model.Product;

import java.text.SimpleDateFormat;

/**
 * Created by sumyntan on 08/10/2016.
 */


    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public interface OnSelectProductListener {
            void onProductSelected(Product product);
        }

        private TextView mTitleTextView;
        private TextView mBodyTextView;
        private Product mProduct;

        public ProductViewHolder(View itemView, final OnSelectProductListener listener) {
            super(itemView);

            mTitleTextView = (TextView) itemView.findViewById(R.id.cell_product_title);
            mBodyTextView = (TextView) itemView.findViewById(R.id.cell_product_body);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onProductSelected(mProduct);
                }
            });

        }

        public void setProductEntry(@NonNull Product product) {
            mProduct = product;
            mTitleTextView.setText(product.getTitle());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String creationDate = simpleDateFormat.format(product.getCreationDate().getTime());

            mBodyTextView.setText(creationDate + " - " + product.getBody());
        }
    }