package com.example.sumyntan.eshop2.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.controller.ProductLoader;
import com.example.sumyntan.eshop2.model.Product;


/**
 * Created by sumyntan on 08/10/2016.
 */


/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ProductViewHolder.OnSelectProductListener {
    private static final int ADD_ENTRY_REQUEST = 1;
    public static final String EXTRA_PRODUCT = "product";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProductListAdapter mProductListAdapter;

    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_product_swipe_refresh);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_product_list_rv_journals);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Swipe Refresh Layout gets new data
        mSwipeRefreshLayout.setOnRefreshListener(this);

        // Prepare layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        // Prepare adapter
        mProductListAdapter = new ProductListAdapter(this);
        mProductListAdapter.setProductList(ProductLoader.loadSamples(getContext()));
        mRecyclerView.setAdapter(mProductListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_add_entry, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            Intent intent = new Intent(getContext(), AddEntryActivity.class);
            startActivityForResult(intent, ADD_ENTRY_REQUEST);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_ENTRY_REQUEST && resultCode == Activity.RESULT_OK) {
            Product product = data.getParcelableExtra(ProductListFragment.EXTRA_PRODUCT);
            mProductListAdapter.addProduct(product);
            mProductListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRefresh() {
        // Intentional delay to simulate a network call
        mSwipeRefreshLayout.getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                mProductListAdapter.notifyDataSetChanged();
            }
        }, 3000);
    }

    public void onProductSelected(Product product) {
        Snackbar.make(getView(), product.getTitle(), Snackbar.LENGTH_SHORT).show();
    }
}