package com.example.sumyntan.eshop2.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.example.sumyntan.eshop2.R;


/**
 * Created by sumyntan on 08/10/2016.
 */

public class AddEntryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entry);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_add_entry_vg_container, new AddEntryFragment())
                .commit();

    }
}
