package com.example.sumyntan.eshop2.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;
import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.model.Product;

/**
 * Created by sumyntan on 08/10/2016.
 */

public class AddEntryFragment extends Fragment {
    private EditText mEntryTitleEditText;
    private EditText mEntryDetailsTextView;
    private Button mAddButton;
    private Button mCancelButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_entry, container, false);

        mEntryTitleEditText = (EditText) rootView.findViewById(R.id.fragment_add_entry_et_title);
        mEntryDetailsTextView = (EditText) rootView.findViewById(R.id.fragment_add_entry_tv_detail);
        mAddButton = (Button) rootView.findViewById(R.id.fragment_add_entry_btn_add);
        mCancelButton = (Button) rootView.findViewById(R.id.fragment_add_entry_btn_cancel);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entryTitle = mEntryTitleEditText.getText().toString();
                String entryDetails = mEntryDetailsTextView.getText().toString();

                Product product = new Product(entryTitle, entryDetails, Calendar.getInstance());

                Intent resultIntent = new Intent();
                resultIntent.putExtra(ProductListFragment.EXTRA_PRODUCT, product);

                getActivity().setResult(Activity.RESULT_OK, resultIntent);
                getActivity().finish();
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        });
    }
}
