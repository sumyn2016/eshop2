package com.example.sumyntan.eshop2.model;

/**
 * Created by sumyntan on 08/10/2016.
 */

import java.util.ArrayList;

public class Laptop {
    private String mName;
    private String mColor;
    private ArrayList<Review> mReviews;

    public Laptop(String name, String location) {
        mName = name;
        mColor = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public ArrayList<Review> getReviews() {
        return mReviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        mReviews = reviews;
    }

    @Override
    public String toString() {
        return getName();
    }

    public double getAverageReview() {
        double average = 0.0;

        for (Review review:mReviews) {
            average += review.getRating();
        }

        return average / (double)mReviews.size();
    }

}
