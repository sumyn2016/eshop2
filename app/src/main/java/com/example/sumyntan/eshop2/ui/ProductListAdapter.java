package com.example.sumyntan.eshop2.ui;

/**
 * Created by sumyntan on 08/10/2016.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.model.Product;

public class ProductListAdapter extends RecyclerView.Adapter {
    private static final int VIEWTYPE_HEADER = 0;
    private static final int VIEWTYPE_CONTENT = 1;

    private final List<Integer> mSortedMonths = new ArrayList<>();
    private List<Product> mProductList = new ArrayList<>();
    private Map<Integer, List<Product>> mSortedProductMap = new HashMap<>();
    private ProductViewHolder.OnSelectProductListener mProductListener;

    public ProductListAdapter(ProductViewHolder.OnSelectProductListener productListener) {
        mProductListener = productListener;
    }

    public void setProductList(List<Product> productList) {
        mProductList = productList;
        sortProductByMonths();
    }

    public void addProduct(Product product) {
        mProductList.add(product);
        sortProductByMonths();
    }

    private void sortProductByMonths() {
        mSortedProductMap.clear();

        for (Product product : mProductList) {
            int month = product.getMonth();

            List<Product> currentMonthProducts = mSortedProductMap.get(month);
            if (currentMonthProducts == null) {
                currentMonthProducts = new ArrayList<>();
                mSortedProductMap.put(month, currentMonthProducts);
            }
            currentMonthProducts.add(product);
        }

        mSortedMonths.clear();
        mSortedMonths.addAll(new ArrayList<Integer>(mSortedProductMap.keySet()));
        Collections.sort(mSortedMonths);
    }

    private Object getItem(int positon) {
        int itemPosition = 0;
        for (int month : mSortedMonths) {
            if (itemPosition == positon) {
                return month;
            }
            itemPosition++;

            List<Product> products = mSortedProductMap.get(month);
            for (Product product : products) {
                if (itemPosition == positon) {
                    return product;
                }

                itemPosition++;
            }
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);

        Object item = getItem(position);
        if (item instanceof Integer) {
            return VIEWTYPE_HEADER;
        }
        return VIEWTYPE_CONTENT;
    }

    @Override
    public int getItemCount() {
        return mProductList.size() + mSortedProductMap.keySet().size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder holder = null;

        switch (viewType) {
            case VIEWTYPE_HEADER:
                View headerView = inflater.inflate(R.layout.cell_header, parent, false);
                holder = new HeaderViewHolder(headerView);
                break;

            case VIEWTYPE_CONTENT:
                View itemView = inflater.inflate(R.layout.cell_product, parent, false);
                holder = new ProductViewHolder(itemView, mProductListener);
                break;
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            Integer month = (Integer) getItem(position);
            Product product = mSortedProductMap.get(month).get(0);

            ((HeaderViewHolder) holder).setHeaderTitle(product);
        } else {
            Product product = (Product) getItem(position);
            ((ProductViewHolder) holder).setProductEntry(product);
        }
    }
}
