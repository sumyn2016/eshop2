package com.example.sumyntan.eshop2.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jkhong on 9/22/16.
 */

public class Product implements Parcelable {
    private static final String BUNDLE_TITLE = "titleBundle";
    private static final String BUNDLE_BODY = "bodyBundle";
    private static final String BUNDLE_CREATION_DATE = "creationDateBundle";
    private static final String BUNDLE_MODIFIED_DATE = "modifiedDateBundle";

    private String mTitle;
    private String mBody;
    private Calendar mCreationDate;
    private Calendar mModifiedDate;

    //region Constructors
    Product() {
        mTitle = "Untitled";
        mCreationDate = Calendar.getInstance();
    }

    public Product(String title, String body, Calendar creationDate) {
        mTitle = title;
        mBody = body;
        mCreationDate = creationDate;
    }
    //endregion

    //region Parcelable
    protected Product(Parcel in) {
        Bundle bundle = in.readBundle();

        mTitle = bundle.getString(BUNDLE_TITLE);
        mBody = bundle.getString(BUNDLE_BODY);
        mCreationDate = (Calendar)bundle.getSerializable(BUNDLE_CREATION_DATE);
        mModifiedDate = (Calendar)bundle.getSerializable(BUNDLE_MODIFIED_DATE);
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_TITLE, mTitle);
        bundle.putString(BUNDLE_BODY, mBody);
        bundle.putSerializable(BUNDLE_CREATION_DATE, mCreationDate);
        bundle.putSerializable(BUNDLE_MODIFIED_DATE, mModifiedDate);

        parcel.writeBundle(bundle);
    }
    //endregion

    //region Getters & Setters
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public Calendar getCreationDate() {
        return mCreationDate;
    }

    public Calendar getModifiedDate() {
        return mModifiedDate;
    }

    public void setModifiedDate(Calendar modifiedDate) {
        mModifiedDate = modifiedDate;
    }
    //endregion

    //region Methods
    public Integer getMonth() {
        return mCreationDate.get(Calendar.MONTH);
    }
    //endregion

}
