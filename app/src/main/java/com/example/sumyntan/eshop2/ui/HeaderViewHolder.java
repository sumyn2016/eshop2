package com.example.sumyntan.eshop2.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.example.sumyntan.eshop2.R;
import com.example.sumyntan.eshop2.model.Product;
/**
 * Created by sumyntan on 08/10/2016.
 */

public class HeaderViewHolder extends RecyclerView.ViewHolder {

    private TextView mHeaderTextView;

    public HeaderViewHolder(View itemView) {
        super(itemView);

        mHeaderTextView = (TextView) itemView.findViewById(R.id.cell_header_tv_month_header);
    }

    public void setHeaderTitle(Product product) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, product.getMonth());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
        mHeaderTextView.setText(simpleDateFormat.format(calendar.getTime()));
    }
}
